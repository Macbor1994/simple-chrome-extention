let inputEl = document.getElementById("input-el");

var listEl = document.querySelector("#list");

let myLeads = [];

let myLeadsStorage = JSON.parse (localStorage.getItem("klucz"));

// if (myLeadsStorage == true) {
//   for (var i = 0; i < array.length; i++) {
//     myLeads += myLeadsStorage
//     renderItems();
//   }

if (myLeadsStorage) {
  myLeads = myLeadsStorage;
  render(myLeads);
}
// function focusFn() {
// }

// function blurFn() {
//   inputEl.value = "Enter site link here...";
//   inputEl.style.fontWeight = "normal";
//   inputEl.style.color = "black";
// }

const saveBtn = document.getElementById("save-btn");
saveBtn.addEventListener("click", function(){
  if (inputEl.value === "Enter site link here..." || inputEl.value === "") {console.log("empty text");return;}
  myLeads.push(inputEl.value);
  inputEl.value = "";
  render(myLeads);

  localStorage.setItem("klucz", JSON.stringify(myLeads));

  console.log(myLeadsStorage);
})

const deleteBtn = document.getElementById("delete-btn");
deleteBtn.addEventListener("dblclick", function(){
  myLeads = [];
  listEl.innerHTML = null;
  localStorage.clear();
  console.log(myLeadsStorage);
})

function render(x) {
  let listItems = "";
  for (var i = 0; i < x.length; i++) {
    listItems += `
      <li>
        <a target='_blank' href='http://${x[i]}'>${x[i]}</a>
      </li>`
  }
  listEl.innerHTML = listItems;
}
